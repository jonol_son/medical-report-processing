import java.util.*;
import java.io.*;

class Mini1
{
	/*******************************main()******************************/
	public static void main(String args[]) throws FileNotFoundException, IOException, Exception
	{
		int choice = 0,k,n;
		Scanner reader = new Scanner(System.in);
		tools tool = new tools();
		tool.loadLists();
		while (choice != -1)
		{
			System.out.println("Enter You Selection:\n1) to loadProgram, \n2) to createArf(makes an arff file) ");
                        System.out.println("3) run fp(runs FP and makes fp file) \n4) wordAssociation (requires #1 and #2 to be run first) ");
                        System.out.println("5) wordLists (requires #1, #2 to be run first.  Also #4 with k=5 (or k5 file in dir))");
                        System.out.println("6) to solve question vi(requires #5 to be run first (or wordlistoutput file in dir))\n\t (-1 to end)");
			choice = reader.nextInt();
			switch(choice)
                        {
                            case 1:
				tool.loadProgram();
                                break;
                            case 2:
				tool.makeArf();
                                break;
                            case 3:
				tool.fpGrowth();
                                break;
                            case 4:
                            	System.out.println("Choose a value for K:");
                                k = reader.nextInt();
                                tool.setK(k);
                                tool.wordAssociation();
                                break;
                            case 5:
                                tool.wordLists();
                                break;
                            case 6:
                                System.out.println("Choose a value for N:");
                                n = reader.nextInt();
                                System.out.println("Choose your subsection (char a,b,c,e,f,g (for g you must modify the family memeber string to choose family member)");
                                char c = reader.next().charAt(0);
                                tool.setN(n);
                                tool.setChar(c);
                                tool.vi();
                                break;
                        }
		}
	}

}


